const AWS = require("aws-sdk")
const { orderBy } = require("lodash")

const s3Endpoint = new AWS.Endpoint("s3.us-west-1.amazonaws.com")

const s3Credentials = new AWS.Credentials({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
})

const s3 = new AWS.S3({
  endpoint: s3Endpoint,
  credentials: s3Credentials,
});

const SUPPORTED_MIME_TYPES = ["image/jpeg", "image/png","video/mp4","video/quicktime"];
const SUPPORTED_FILE_EXTENSIONS = ["jpeg", "jpg", "png","mp4","mov"];

const BUCKET_NAME = "multipart-mobile-poc";

const UploadController = {
  createMultipartUpload: async (req, res) => {
    console.log("createMultipartUpload called", req.body);

    const { name, mimeType } = req.body
    if(!name && !mimeType){
      res.send('name and mimeType are required');
    }
    const ext = name.split(".").pop();

    if (!SUPPORTED_FILE_EXTENSIONS.includes(ext)) {
      res.send({
        error: "unsupported file extension",
      })
    } else if (!SUPPORTED_MIME_TYPES.includes(mimeType)) {
      res.send({
        error: "unsupported mime type",
      })
    }

    const multipartParams = {
      Bucket: BUCKET_NAME,
      Key: `${name}`,
      // ACL: "public-read",
      Metadata:{
        mimeType: mimeType
      }
    }
    try {
      const multipartUpload = await s3.createMultipartUpload(multipartParams).promise()

      res.send({
        UploadId: multipartUpload.UploadId,
        fileKey: multipartUpload.Key,
      })
    } catch (e) {
      console.error(e.message);
      res.send({e});
    }
  },

  getMultipartPreSignedUrls: async (req, res) => {
    console.log("getMultipartPreSignedUrls called", req.body);

    const { fileKey, UploadId, parts } = req.body

    const multipartParams = {
      Bucket: BUCKET_NAME,
      Key: fileKey,
      UploadId: UploadId,
    }

    const promises = []

    for (let index = 0; index < parts; index++) {
      promises.push(
        s3.getSignedUrlPromise("uploadPart", {
          ...multipartParams,
          PartNumber: index + 1,
        }),
      )
    }

    const signedUrls = await Promise.all(promises)

    const partSignedUrlList = signedUrls.map((signedUrl, index) => {
      return {
        signedUrl: signedUrl,
        PartNumber: index + 1,
      }
    })

    res.send({
      parts: partSignedUrlList,
    })
  },

  completeMultipartUpload: async (req, res) => {
    const traceId = Date.now();
    console.log("completeMultipartUpload called",traceId, req.body);

    const { UploadId, fileKey, parts } = req.body

    const multipartParams = {
      Bucket: BUCKET_NAME,
      Key: fileKey,
      UploadId: UploadId,
      MultipartUpload: {
        // ordering the parts to make sure they are in the right order
        Parts: orderBy(parts, ["PartNumber"], ["asc"]),
      },
    }

    res.setHeader('traceId',traceId);
    try {
      await s3.completeMultipartUpload(multipartParams).promise()
      res.send()
    } catch (error) {
      res.send({ error })
    }
  },
}

module.exports = { UploadController }
